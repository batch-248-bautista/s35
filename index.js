const express = require("express");

// monggose is a package that allows the creation of schemas to model our data structure
//ongoos is an ODM librariesto let our exoress js API manipulate a mongodb database
const mongoose = require("mongoose");

const app = express();

const port = 4000;

//mongoDB Connection
//connect to the databse by passing in our  connection string(robo3t)
//remeber to replace the password and datbase name with actual values

//syntax connect mongodb to server we are creating todday
	//mongiise.conect("<MongoDB Connection String>",{options to avoid errors in our connection})
	//"kukunin sa mongodb connection"

//connecting to mongodb atlas
mongoose.set('strictQuery', true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.jeupjjp.mongodb.net/s35?retryWrites=true&w=majority",{
	//Allows us to avoid any current and futrue errors while connecting to MongoDB
	useNewUrlParser: true,
	useUnifiedTopology: true,
})

//see notifications for connection succes or failure
//connection to databse
// allows us to handle errors when inital connection is established
//works with on and one mongoose methods

let db = mongoose.connection;

//error handling in connecting
db.on("error", console.error.bind(console,"connection error"));
//this will be triggered if the connection is successful
db.once("open",()=>console.log("We're connected to MongoDB Atlas!"))

//Mongoose Schema
	//schemas act as blueprints to our data
	//Syntax
		//const schemaName = new mongoose.Schema({keyvaluepairs})
		//use hte schmea() construcotr of the mongoose module to create a new schema object
		//the "new" keyword create a brandnew schema
		//required, itis used to specify that a field must ntot be empty
		//default, it is used if the field value is not supllied
	const taskSchema = new mongoose.Schema({
		name :{
			type: String,
			required:[true, "Task name is required!"]
		},
		status: {
			type : String,
			default: "pending"
		}
	})


	const userSchema = new mongoose.Schema({
		username :{
			type: String,
			required:true
		},
		password: {
			type : String,
			required:true
		}
	})

//MODELS
	//models uses schema and are use to create/instatiate objects that correspond to our schemna
	// models use schema and theyact as middleman from the server to our database
	// server>schema(blueprint>databse>collection(Models)

	//first parameter - collection where to store the data

	// second param - specify the schema/blueprint of the documents that will be store in our database

	const Task = mongoose.model("Task", taskSchema)

	const User = mongoose.model("User", userSchema)


	//business logic un creating a new task
	/*
	1. Add a funtionality to check if there are duplicate tasks
		-if the task already exists in the db, we will return an error
		- if thetask doesn't exist in the db, we add it in db
	2.the task will be coming from our request body
	3. cretae a new task ovject with only a name field/property
	4. the "status" property does not need to be provided beacuse our schema defaults return it to "pending" upon creation of an object.



	*/
	//set up forallowinng the server to handle data from rquests
//allows your application to read json data
app.use(express.json());

//allow our app to read data from forms
app.use(express.urlencoded({extended:true}));
	

	app.post("/tasks",(req, res)=>{
		
		Task.findOne({name:req.body.name}, (err, result)=>{
			if(result != null && result.name == req.body.name){
				return res.send(`Duplicate task found!`)
			}
			else{
				let newTask = new Task({
					name: req.body.name
				})
				newTask.save((saveErr, savedTask)=>{
					if(saveErr){
						return console.error(saveErr)
					}
					else{
						console.log(`${newTask} is saved!`);
						return res.status(201).send("New Task created")
					}
				})
			}
		})
	})

	app.post("/signup",(req, res)=>{
		if(!req.body.password || !req.body.username){
		return res.send("Username and password are required.")
	}

		User.findOne({username:req.body.username, password:req.body.password}, (err, result)=>{
			if(result != null && result.username == req.body.username){
				return res.send(`Duplicate username found!`)
			}
			else{
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})
				newUser.save((saveErr, savedUser)=>{
					if(saveErr){
						return console.error(saveErr)
					}
					else{
						console.log(`New user registered!`);
						return res.status(201).send("New user registered.")
					}
				})
			}
		})
	})

	//Getting all the tasks
	// business logic
	/*
	1. Retrieve all the documents
	2.If an error is encounterers, print the error
	3. if there ae no error foi=und, send a succes status back to the client/postman and return array of documents
	*/
	app.get("/tasks", (request, response)=>{
		Task.find({}, (error, result)=>{
			if (error){
				console.log(error);
			}
			else{
				return response.send(result)
			}
		})
	})

	app.get("/users", (request, response)=>{
		User.find({}, (error, result)=>{
			if (error){
				console.log(error);
			}
			else{
				return response.send(result)
			}
		})
	})

// activity
//schema
	
//model












//listen to the port, meaningm ifthe port accsed, we run the server.
app.listen(port,()=>console.log(`Server running at port ${port}.`));

